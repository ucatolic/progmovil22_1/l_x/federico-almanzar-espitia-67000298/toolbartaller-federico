package com.example.toolbar

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem)= when(item.itemId){
        R.id.crear->{
            startActivity(Intent(this,Herramientas::class.java))
            true
        }
        R.id.overflow_menu->{
            startActivity(Intent(this,Overflow::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

}